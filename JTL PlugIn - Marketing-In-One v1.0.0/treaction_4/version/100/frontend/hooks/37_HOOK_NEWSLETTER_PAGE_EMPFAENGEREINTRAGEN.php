<?php

$oNewsletterEmpfaenger = &$args_arr['oNewsletterEmpfaenger'];

$tr_data = Shop::DB()->select('xplugin_treaction_4_data', 'kID', 1);

if ($tr_data->bAccountStatusVerified) {

    // formData for Newsletter Webhook
    $formData = [
        'base' => [
            'apikey' => $tr_data->cAPIKey,
            'account_number' => $tr_data->cAccountNumber,
            'object_register_id' => $tr_data->cNewsletterHookId,
        ],
        'contact' => [
            'standard' => [
                [
                    'salutation' => $oNewsletterEmpfaenger->cAnrede === 'm' ? 'Herr' : 'Frau',
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'first_name' => $oNewsletterEmpfaenger->cVorname,
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'last_name' => $oNewsletterEmpfaenger->cNachname,
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'email' => $oNewsletterEmpfaenger->cEmail,
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
            ],
        ],
    ];

    // JSON only works with UTF-8 encoded material!
    array_walk_recursive($formData, static function (&$val, $key) {
        $val = utf8_encode($val);
    });

    $ch = curl_init('https://dev-api-in-one.net/contact/create');
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, base64_encode(json_encode($formData)));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: text/html',
        'Access-Control-Allow-Origin: *',
        'Access-Control-Allow-Methods: PUT, GET, POST, DELETE',
        'Access-Control-Allow-Headers: access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer',
        'Access-Control-Max-Age: 86400',
    ));
    $result = curl_exec($ch);
    curl_close($ch);
}
