<?php

// Define global properties
$oBestellung = &$args_arr['oBestellung'];
$bestellID = &$args_arr['bestellID'];
$bestellstatus = &$args_arr['bestellstatus'];

// Get current order
$curr_order_id = $oBestellung->kBestellung;
$curr_order = new Bestellung($curr_order_id);
$curr_order->fuelleBestellung();

// Get customer
$kunde = $curr_order->oKunde;

// Get database instance
$db = NiceDB::getInstance();

// Get current year's orders
$query = 'SELECT * FROM tbestellung WHERE kKunde = :customer_id AND YEAR(dErstellt) = :current_year';
$params = [
    'customer_id' => $kunde->kKunde,
    'current_year' => date('Y'),
];
$current_year_orders = $db->executeQueryPrepared($query, $params, 2) ?: null;

// Get current year's order net value
$current_year_orders_net_value = 0;
foreach ($current_year_orders as &$order) {
    $order = (new Bestellung($order->kBestellung))->fuelleBestellung();
    // Use "fWarensummeNetto" instead if you want the net value without shipping costs !
    $current_year_orders_net_value += (float)number_format($order->fGesamtsummeNetto, 2);
}
unset($order);

// Get all year's orders
$query = 'SELECT * FROM tbestellung WHERE kKunde = :customer_id';
$params = [
    'customer_id' => $kunde->kKunde
];
$all_year_orders = $db->executeQueryPrepared($query, $params, 2) ?: null;

// Get all year's order net value
$all_year_orders_net_value = 0;
foreach ($all_year_orders as &$order) {
    $order = (new Bestellung($order->kBestellung))->fuelleBestellung();
    // Use "fWarensummeNetto" instead if you want the net value without shipping costs !
    $all_year_orders_net_value += (float)number_format($order->fGesamtsummeNetto, 2);
}
unset($order);

// BEGIN: Get category names list
// 1. Get order's article ids
$order_articles_ids = [];
foreach ($curr_order->Positionen as $Position) {
    if ($Position->Artikel !== null) {
        $order_articles_ids[] = $Position->Artikel->kArtikel;
    }
}

// 2. Get category ids from mapping table by article ids
$query = 'SELECT kKategorie FROM tkategorieartikel WHERE kArtikel IN (' . implode(',', $order_articles_ids) . ')';
$category_articles = $db->executeQuery($query, 9) ?: null;
$category_ids = [];
foreach ($category_articles as $category_article) {
    $category_ids[$category_article['kKategorie']] = $category_article['kKategorie'];
}

// 3. Add parent category ids to $category_ids
$zaehler = 0;
function addCategoryParents(&$category_ids, $db)
{
    global $zaehler;
    $zaehler++;
    $cnt = 0;
    $query = 'SELECT * FROM tkategorie WHERE kKategorie IN ('
        . implode(',', $category_ids) . ') AND kOberKategorie != 0';
    $categories = $db->executeQuery($query, 9) ?: null;
    foreach ($categories as $category) {
        if (!array_key_exists($category['kOberKategorie'], $category_ids)) {
            $category_ids[$category['kOberKategorie']] = $category['kOberKategorie'];
            $cnt++;
        }
    }
    if ($cnt > 0) {
        addCategoryParents($category_ids, $db);
    }
}

addCategoryParents($category_ids, $db);

// 4. Get category names by category ids
$query = 'SELECT kKategorie, cName FROM tkategorie WHERE kKategorie IN (' . implode(',', $category_ids) . ')';
$categories = $db->executeQuery($query, 9) ?: null;

// 5. Create list of smart tags and array of parent categories
$list_of_smart_tags = '';
$array_of_smart_tags = [];
foreach ($categories as $category) {
    // Use associative array's key to prevent duplicates!
    $array_of_smart_tags[$category['cName']] = $category['cName'];
}
$list_of_smart_tags = implode(',', $array_of_smart_tags);
// END: Get category names list

// Create data for microservice
$tr_data = Shop::DB()->select('xplugin_treaction_4_data', 'kID', 1);

if ($tr_data->bAccountStatusVerified) {

    $title = '';
    if (!empty($kunde->cTitel)) {
        $title = ' ' . $kunde->cTitel;
    }

    // formData for eCommerce Webhook
    $formData = [
        'base' => [
            'apikey' => $tr_data->cAPIKey,
            'account_number' => $tr_data->cAccountNumber,
            'object_register_id' => $tr_data->cECommerceHookId,
        ],
        'contact' => [
            'standard' => [
                [
                    'salutation' => $kunde->cAnredeLocalized . $title,
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'first_name' => $kunde->cVorname,
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'last_name' => $kunde->cNachname,
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'email' => $kunde->cMail,
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'street' => $kunde->cStrasse,
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'house_number' => $kunde->cHausnummer,
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'postal_code' => $kunde->cPLZ,
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'city' => $kunde->cOrt,
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'country' => $kunde->cLand,
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'last_order_date' => date_format(date_create($curr_order->dErstellt), 'd-M-Y'),
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'smart_tags' => $list_of_smart_tags,
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    // Use "fWarensummeNetto" instead if you want the net value without shipping costs !
                    'last_order_net_value' => number_format($curr_order->fGesamtsummeNetto, 2),
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    // Use "fWarensummeNetto" instead if you want the net value without shipping costs !
                    'total_order_net_value' => $all_year_orders_net_value,
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'last_year_order_net_value' => $current_year_orders_net_value,
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'last_order_no' => $curr_order->cBestellNr,
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'referrer_id' => htmlspecialchars($_GET['referrer_id']),
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'affiliate_id' => '',           // not available
                    'required' => 'false',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'affiliate_sub_id' => '',       // not available
                    'required' => 'false',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
                [
                    'url' => \Shop::getURL() . $_SERVER['REQUEST_URI'],
                    'required' => 'true',
                    'datatype' => 'Text',
                    'regex' => '',
                ],
            ],
        ],
    ];

    // JSON only works with UTF-8 encoded material!
    array_walk_recursive($formData, static function (&$val, $key) {
        $val = utf8_encode($val);
    });

    $ch = curl_init('https://dev-api-in-one.net/contact/create');
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, base64_encode(json_encode($formData)));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: text/html',
        'Access-Control-Allow-Origin: *',
        'Access-Control-Allow-Methods: PUT, GET, POST, DELETE',
        'Access-Control-Allow-Headers: access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer',
        'Access-Control-Max-Age: 86400',
    ));
    $result = curl_exec($ch);
    curl_close($ch);
}
