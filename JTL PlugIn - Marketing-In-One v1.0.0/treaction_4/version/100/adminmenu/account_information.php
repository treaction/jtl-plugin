<?php

/** @var Smarty $smarty */
/** @var Plugin $oPlugin */

// Extend $oPlugin with the plugin path
$oPlugin->cBackendUrl = \Shop::getURL() . '/' . PFAD_ADMIN . 'plugin.php?kPlugin=' . $oPlugin->kPlugin;

// Get account data from DB
$tr_data = Shop::DB()->select('xplugin_treaction_4_data', 'kID', 1);

// Reset values - For testing only !
//$tr_data->cAccountNumber = '';
//$tr_data->cAPIKey = '';
//$tr_data->bAccountStatusVerified = 0;
//Shop::DB()->update(
//    'xplugin_treaction_4_data',
//    'kID',
//    1,
//    $tr_data
//);

// Save verified account data to DB
if (
    isset(
        $_POST['api_key'],
        $_POST['acc_number'],
        $_POST['newsletter_hook_id'],
        $_POST['ecommerce_hook_id'],
        $_POST['acc_verified']
    ) && $_POST['acc_verified'] === '1'
) {
    $tr_data->cAccountNumber = $_POST['acc_number'];
    $tr_data->cAPIKey = $_POST['api_key'];
    $tr_data->cNewsletterHookId = $_POST['newsletter_hook_id'];
    $tr_data->cECommerceHookId = $_POST['ecommerce_hook_id'];
    $tr_data->bAccountStatusVerified = '1';
    Shop::DB()->update(
        'xplugin_treaction_4_data',
        'kID',
        1,
        $tr_data
    );
}

// Set properties for smarty template shown in plugin settings tab
$smarty->assign('tr_data', $tr_data);

// Set smarty template to show in plugin settings tab
$smarty->display($oPlugin->cAdminmenuPfad . 'template/account_information.tpl');