<div class="tr_container">

    <div class="tr_header py-5 text-center">
        <img class="d-block mx-auto mb-4" style="background-color:black"
             src="https://dev-campaign-in-one.net/assets/img/cio/cio-logo-weiss-150x150.png"
             alt="" width="72" height="72">
        <h2>MVP Shop Plugin</h2>
        <p class="lead">Check your Marketing-In-One Account / APIKey</p>
    </div>

    <br>

    <!--Begin: Account Infomation-->
    <div class="card" style="margin-bottom: 15px;">
        <div class="card-header">
            <br>
            <div class="row">
                <div class="col-md-12 order-md-1">
                    <h3 class="text-center mt-0">Account Information</h3>
                    <br>
                    <form class="needs-validation" enctype="multipart/form-data" method="post" role="form"
                          accept-charset="utf-8" id="tr_verify_account_form">

                        <!--Begin : APIKey and Account Number  -->
                        <div class="row">
                            <div class="mb-4 col-md-6">
                                <label for="tst-apikey" class="text-left w-100">APIKey</label>
                                <input type="text" name="tst-api-key" class="acc_info_input form-control" id="tst-apikey"
                                       value="{$tr_data->cAPIKey}" placeholder="" required>
                                <small class="form-text text-muted">
                                    Please follow this link to find your <a href="https://dev-campaign-in-one.net?redirect=YWNjLmdldFNldHRpbmdzR2VuZXJhbA==" target="_blank">APIKey</a>
                                </small>
                            </div>

                            <div class="mb-4 col-md-6 ">
                                <label for="tst-acc-no" class="text-left w-100">Account Number</label>
                                <input type="number" name="tst-acc-no" class="acc_info_input form-control" id="tst-acc-no"
                                       value="{$tr_data->cAccountNumber}" placeholder="" required>
                                <small class="form-text text-muted">
                                    Please follow this link to find your <a href="https://dev-campaign-in-one.net?redirect=YWNjLmdldHVwZGF0ZQ==" target="_blank">Account Number</a>
                                </small>
                            </div>
                        </div>
                        <!--End : APIKey and Account Number  -->

                        <!--Begin : Drop Down for Webhook -->
                        <!--Should be seen only after the APIKey and Account No is right. -->
{*                        <div class="row" id="div-wb-lst">*}
{*                            <div class="mb-4 col-md-6">*}
{*                                <label for="sel-gt-nws-wb" class="text-left w-100">NewsLetter Webhook</label>*}
{*                                <select class="form-control" id="sel-gt-nws-wb" name="newsletter_hook_id">*}
{*                                    <option selected disabled></option>*}
{*                                </select>*}
{*                                <input type="button" value="Test Newsletter Webhook" id="tst-nws-wbh">*}
{*                            </div>*}
{*                            <div class="mb-4 col-md-6">*}
{*                                <label for="sel-gt-ecm-wb" class="text-left w-100">Ecommerce Webhook</label>*}
{*                                <select class="form-control" id="sel-gt-ecm-wb" name="ecommerce_hook_id">*}
{*                                    <option selected disabled></option>*}
{*                                </select>*}
{*                                <input type="button" value="Test eCommerce Webhook" id="tst-ecm-wbh">*}
{*                            </div>*}
{*                        </div>*}
                        <input type="hidden" name="newsletter_hook_id" value="" id="newsletter_hook_id">
                        <input type="hidden" name="ecommerce_hook_id" value="" id="ecommerce_hook_id">
                        <!--End : Drop Down for Webhook -->

                        <div class="row">
                            <div class="col-md-6">
                                <div id="tst-akey-er-msg" class="row" style="display: flex;">
                                    <div>
                                        <a data-html="true" class="cio-popover" data-toggle="popover"
                                           title="API status" data-content="">
                                            <i style="color: #eb4034;" class="fa fa-circle fa-lg checkButton"></i>
                                        </a>
                                    </div>
                                    <div class="">
                                        <div class="alert-cioapikey text-hide" role="alert">&nbsp;</div>
                                    </div>
                                </div>
                                <div class="row" id="row-gt-wb-lst-loader" style="display: flex;">
                                    <div class="loader"></div>
                                    <label>Validating APIKey</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row" id="tr_acc_data_saved" style="display: flex;">
                                    <div>
                                        <a>
                                            <i style="color: #16eb1e;" class="is_saved_bullet fa fa-circle fa-lg"></i>
                                            <i style="color: #eb101f;" class="is_not_saved_bullet fa fa-circle fa-lg"></i>
                                        </a>
                                    </div>
                                    <div class="">
                                        <div class="is_saved_msg">&nbsp;Account data is saved.</div>
                                        <div class="is_not_saved_msg">&nbsp;Account data is not saved.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>

                        <!--Begin : Buttons TestAPIKey and Save -->
                        <div class="row">
                            <div class="mb-4 col-md-6">
                                <input class="btn btn-primary btn-block btn-submit" id="bt-tst-cnf"
                                       value="Verify APIKey" type="submit">
                            </div>
                            <div class="mb-4 col-md-6">
                                <input class="btn btn-primary btn-block btn-submit" id="bt-sve-cnf"
                                       value="Save" type="button">
                            </div>
                        </div>
                        <!--End : Buttons TestAPIKey and Save -->

                    </form>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <!--End Account Information-->

    <!--Begin : Footer -->
    <div class="tr_footer text-center text-muted">
        <p>&copy; 2020 treaction AG</p>
        <ul class="list-inline">
            <li class="list-inline-item">
                <a target="_blank" href="https://treaction.net/rechtliches/#impressum">Imprint</a>
            </li>
            <li class="list-inline-item">
                <a target="_blank" href="https://treaction.net/rechtliches/#agb">Conditions</a>
            </li>
            <li class="list-inline-item">
                <a target="_blank" href="https://treaction.net/rechtliches/#datenschutz">Privacy</a>
            </li>
            <li class="list-inline-item">
                <a target="_blank" href="https://treaction.net/kontakt/">Contact</a>
            </li>
        </ul>
    </div>
    <!--End : footer-->

    <script>
        $(document).ready(function () {

                //********************************************************************************
                // Global properties
                //********************************************************************************

                let button_save = $('#bt-sve-cnf');
                let button_verify = $('#bt-tst-cnf');
                let loader = $('#row-gt-wb-lst-loader');
                let is_saved_msg = $('.is_saved_msg');
                let is_saved_bullet = $('.is_saved_bullet');
                let is_not_saved_bullet = $('.is_not_saved_bullet');
                let is_not_saved_msg = $('.is_not_saved_msg');
                let input_api_key = $('#tst-apikey');
                let input_acc_no = $('#tst-acc-no');
                let inputs_account_info = $('.acc_info_input');
                let status_information_container = $('#tst-akey-er-msg');
                let status_information_bullet = $('.checkButton');
                let status_information_msg = $('.alert-cioapikey');
                let form_verify_account = $('#tr_verify_account_form');

                // ********************************************************************************
                // Helper functions
                // ********************************************************************************

                /**
                 * Get or change the account data save status
                 */
                function accountDataSaved(status = false) {
                    if (status) {
                        is_not_saved_msg.hide();
                        is_not_saved_bullet.hide();
                        is_saved_msg.show();
                        is_saved_bullet.show();
                    } else {
                        is_saved_msg.hide();
                        is_saved_bullet.hide();
                        is_not_saved_msg.show();
                        is_not_saved_bullet.show();
                    }
                    return status;
                }

                /**
                 * If account data is already verified and saved:
                 * - Show relevant elements
                 * - Hide save button
                 */
                function initialSetUp() {
                    let data_verified = '{$tr_data->bAccountStatusVerified}' === '1';
                    loader.hide();
                    if (data_verified) {
                        checkAPIKey('{$tr_data->cAccountNumber}', '{$tr_data->cAPIKey}').then(function(status) {
                            if (status) {
                                accountDataSaved(true);
                            } else {
                                accountDataSaved(false);
                                showStatusMessageIsNotValidated();
                            }
                        });
                    } else {
                        accountDataSaved(false);
                        showStatusMessageIsNotValidated();
                    }
                    button_save.hide();
                }

                /**
                 * Check APIKey and add webhook list to selects
                 */
                function checkAPIKey(accNo, apikey) {
                    let deferred = $.Deferred();

                    status_information_container.hide();
                    loader.show();
                    button_verify.prop("disabled", true);

                    if (!accNo || !apikey) {
                        loader.hide();
                        status_information_container.show();
                        button_verify.prop("disabled", false);
                        return false;
                    }

                    let formData = {
                        "base": {
                            'account_no': accNo,
                            'apikey': apikey
                        }
                    };
                    let formJson = JSON.stringify(formData);
                    let formbtoa = btoa(formJson);

                    $.ajax({
                        url: "https://dev-api-in-one.net/webhook/list/read",
                        type: 'POST',
                        async: false,
                        timeout: 0,
                        header: {
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE',
                            'Access-Control-Allow-Headers': 'access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer',
                            'Access-Control-Max-Age': '86400'
                        },
                        data: { data: formbtoa },
                        success: function (data) {
                            let response = (data);
                            if (response.status) {
                                // APIKey successfully validated
                                let options = response.response;
                                $.each(options, function (key, value) {
                                    status_information_bullet.css({ 'color': '#16eb1e' }).show();
                                    if (value['name'] === 'NewsletterHook') {
                                        $('#newsletter_hook_id').val(value['objectregister_id']);
                                    } else if (value['name'] === 'eCommerceHook') {
                                        $('#ecommerce_hook_id').val(value['objectregister_id']);
                                    }
                                });
                                status_information_msg.empty().append(response.message);
                                status_information_msg.removeClass('text-hide');
                                button_verify.hide();
                                button_save.show();
                                deferred.resolve(true);
                            } else {
                                // APIKey validation failed
                                button_save.hide();
                                button_verify.show();
                                status_information_bullet.css({ 'color': '#eb101f' }).show();
                                status_information_msg.empty().append(response.message);
                                status_information_msg.removeClass('text-hide');
                                deferred.resolve(false);
                            }
                        },
                        error: function (data) {
                            button_save.hide();
                            button_verify.show();
                            status_information_bullet.css({ 'color': '#eb101f' });
                            deferred.resolve(false);
                        }
                    }).always(function () {
                        loader.hide();
                        status_information_container.show();
                        button_verify.prop("disabled", false);
                    });

                    return deferred.promise();
                }

                function showStatusMessageIsValidated() {
                    status_information_bullet.css('color', '#16eb1e').show();
                    status_information_msg.empty().append('Account data is validated.').removeClass('text-hide');
                }

                function showStatusMessageIsNotValidated() {
                    status_information_bullet.css('color', '#eb101f').show();
                    status_information_msg.empty().append('Account data is not validated.').removeClass('text-hide');
                }

                //********************************************************************************
                // Listeners
                //********************************************************************************

                // Check APIKey and get list of webhooks
                form_verify_account.on('submit', function (e) {
                    e.preventDefault();
                    if (button_verify.is(":visible")) {
                        checkAPIKey(input_acc_no.val(), input_api_key.val());
                    }
                });

                // If inputs are changed, hide relevant elements
                inputs_account_info.on('change', function (e) {
                    let sAPIKey = '{$tr_data->cAPIKey}';
                    let sAccNo = '{$tr_data->cAccountNumber}';
                    let cAPIKey = input_api_key.val();
                    let cAccNo = input_acc_no.val();
                    button_save.hide();
                    if (((cAPIKey !== '') && (cAccNo !== '')) && ((sAPIKey === cAPIKey) && (sAccNo === cAccNo))) {
                        button_verify.hide();
                        showStatusMessageIsValidated();
                        accountDataSaved(true);
                        return;
                    }
                    button_verify.show();
                    showStatusMessageIsNotValidated();
                    accountDataSaved(false);
                });

                // Save account data when verified
                button_save.on('click', function () {
                    let form = $('<form></form>');
                    form.attr('action', '{$oPlugin->cBackendUrl}');
                    form.attr('method', 'post');
                    form.append($('<input type="hidden" name="api_key" value="' + input_api_key.val() + '">'));
                    form.append($('<input type="hidden" name="acc_number" value="' + input_acc_no.val() + '">'));
                    form.append($('#newsletter_hook_id'));
                    form.append($('#ecommerce_hook_id'));
                    form.append($('<input type="hidden" name="acc_verified" value="1">'));
                    form.appendTo('body').submit();
                });

                //********************************************************************************
                // Init
                //********************************************************************************

                initialSetUp();

            }
        );
    </script>
</div>