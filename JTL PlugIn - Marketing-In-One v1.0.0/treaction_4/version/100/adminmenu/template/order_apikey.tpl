<style>

    /* Styles */

    .tr_container {
        /*width: 80%;*/
        margin: 15px auto;
        font-family: -apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
        font-weight: 400;
        line-height: 1.5;
        color: #292b2c;
    }

    .tr_container h1,
    .tr_container h2,
    .tr_container h3,
    .tr_container h4,
    .tr_container h5,
    .tr_container h6 {
        margin-bottom: .5rem;
        font-family: inherit;
        font-weight: 500;
        line-height: 1.1;
        color: inherit;
    }

    .tr_container p {
        margin-top: 0;
        margin-bottom: 1rem;
    }

    .tr_container a {
        color: #0275d8;
        text-decoration: none;
    }

    .tr_container a:focus,
    .tr_container a:hover {
        color: #014c8c;
        text-decoration: underline;
    }

    .tr_container .text-center {
        text-align: center;
    }

    .tr_container .mt-0 {
        margin-top: 0;
    }

    .tr_container .card {
        position: relative;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, .125);
        border-radius: .25rem;
    }

    .tr_container .card-header:first-child {
        border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
    }

    .tr_container .card-header {
        padding: .75rem 1.25rem;
        margin-bottom: 0;
        background-color: #f7f7f9;
        border-bottom: 1px solid rgba(0, 0, 0, .125);
    }

    .tr_container .text-muted {
        color: #636c72 !important;
    }

    .tr_container dl,
    .tr_container ol,
    .tr_container ul {
        margin-top: 0;
        margin-bottom: 1rem;
    }

    .tr_container .list-inline {
        padding-left: 0;
        list-style: none;
    }

    .tr_container .list-inline-item:not(:last-child) {
        margin-right: 5px;
    }

    .tr_container .list-inline-item {
        display: inline-block;
    }

    .tr_container input[type="button"].btn-block,
    .tr_container input[type="reset"].btn-block,
    .tr_container input[type="submit"].btn-block {
        width: 100%;
    }

    .tr_container .btn-block {
        display: block;
        width: 100%;
    }

    .tr_container .btn {
        display: inline-block;
        font-weight: 400;
        line-height: 1.25;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        border: 1px solid transparent;
        padding: .5rem 1rem;
        border-radius: .25rem;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
        transition: all .2s ease-in-out;
    }

    .tr_container .form-control:focus {
        color: #464a4c;
        background-color: #fff;
        border-color: #5cb3fd;
        outline: 0;
    }

    .tr_container .form-control {
        display: block;
        width: 100%;
        padding: .5rem .75rem;
        line-height: 1.25;
        color: #464a4c;
        background-color: #fff;
        background-image: none;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        border: 1px solid rgba(0, 0, 0, .15);
        border-radius: .25rem;
        -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    }

    .tr_container .row .mb-4 {
        margin-bottom: 15px;
    }

    .tr_container .row label {
        display: block;
    }

    .tr_container .text-left {
        text-align: left;
    }

    .tr_container .w-100 {
        width: 100%;
    }

    label {
        font-weight: normal;
    }

    /******* style.css ********/

    .tr_container .btn-primary {
        background-color: #7abe73;
        color: #fff;
        border-color: #7abe73;
        cursor: pointer;
    }

    .tr_container .btn-primary:hover {
        color: #fff;
        border-color: #7abe73;
        background-color: #61955b;
    }

    .tr_container #tst-akey-er-msg,
    .tr_container #tr_acc_data_saved {
        padding-top: 7px;
        padding-left: 15px;
    }

    .tr_container #tst-akey-er-msg-nws {
        padding-top: 41px;
        padding-left: 20px;
    }

    .tr_container div.alert-cioapikey,
    .tr_container #tr_acc_data_saved .is_saved,
    .tr_container #tr_acc_data_saved .is_not_saved{
        padding-left: 5px;
    }

    .tr_container footer.footer {
        /*    background: #292b2c;
            margin-top: 50px;*/
    }

    .tr_container .loader {
        border: 5px solid #7abe73; /* Light grey */
        border-top: 5px solid #61955b; /* Blue */
        border-radius: 50%;
        width: 30px;
        height: 30px;
        animation: spin 2s linear infinite;
        margin-right: 10px;
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }

    .tr_container div#row-gt-wb-lst-loader {
        padding: 0px 15px;
    }

    .tr_container #tst-nws-wbh,
    .tr_container #tst-ecm-wbh {
        margin-top: 7px;
    }

    .tr_container label.btn.active, .btn:focus {
        background: #292b2c !important;
        color: #fff !important;
    }

    .tr_container input.btn.focus, .btn:focus {
        background: #7abe73 !important;
    }
</style>
<div class="tr_container">

    <div class="tr_header py-5 text-center">
        <img class="d-block mx-auto mb-4" style="background-color:black"
             src="https://dev-campaign-in-one.net/assets/img/cio/cio-logo-weiss-150x150.png"
             alt="" width="72" height="72">
        <h2>MVP Shop Plugin</h2>
        <p class="lead">Create your Marketing-In-One Account / APIKey</p>
    </div>

    <br>

    <!-- Begin Get APIKey (New/Existing) for User -->
    <div class="card" style="margin-bottom:15px;" id="card-gt-apikey">
        <div class="card-header">
            <br>
            <!--Begin : New User Registration -->
            <div class="row" id="div-nw-cus">
                <div class="col-md-12 order-md-1">
                    <div class="py-1 text-center">
                        <h3 class="text-center mt-0">Marketing-In-One Registration</h3>
                        <br>

                        <form class="needs-validation" enctype="multipart/form-data" method="post" role="form"
                              accept-charset="utf-8">

                            <div class="row">
                                <div class="mb-4 col-md-6">
                                    <label for="nw-cus-sal" class="text-left w-100">Salutation</label>
                                    <select class="form-control" name="salutation" id="nw-cus-sal">
                                        <option class="form-control" value="Mr">Mr</option>
                                        <option class="form-control" value="Mrs">Mrs</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <!--FirstName-->
                                <div class="mb-4 col-md-6">
                                    <label for="nw-cus-fstnme" class="text-left w-100">First Name</label>
                                    <input type="text" name="firstName" class="form-control" id="nw-cus-fstnme"
                                           placeholder="" required>
                                </div>
                                <!--LastName-->
                                <div class="mb-4 col-md-6">
                                    <label for="nw-cus-lstnme" class="text-left w-100">Last Name</label>
                                    <input type="text" name="lastLame" class="form-control" id="nw-cus-lstnme"
                                           placeholder="" required>
                                </div>
                            </div>

                            <div class="row">
                                <!--Email-->
                                <div class="mb-4 col-md-6">
                                    <label for="nw-cus-em" class="text-left w-100">E-Mail</label>
                                    <input type="email" name="email" class="form-control" id="nw-cus-em"
                                           placeholder="" required>
                                </div>
                            </div>

                            <br>

                            <div class="card" style="margin-bottom:15px;">
                                <div class="card-header">
                                    <label class="mb-4" style="margin-bottom: 5px;">
                                        <input type="checkbox" id="nw-cus-permission" required>
                                        <small>&nbsp;&nbsp;Ich erkläre mich mit den
                                            <a href="https://treaction.net/rechtliches/#agb" target="_blank">AGB</a>
                                            und
                                            <a href="https://treaction.net/rechtliches/#datenschutz"
                                                    target="_blank">Datenschutzbestimmungen</a>
                                            der treaction AG, Kurfürstenstr. 1, 34117 Kassel, einverstanden. Ich bin
                                            einverstanden, dass die treaction AG meine eingetragenen Daten nutzt und mir
                                            E-Mails zuschickt bzw. mich anruft, um mir Angebote/Beratung zu den Bereichen
                                            Lead-Generierung, Marketing-Automation oder E-Mail-Marketing zukommen zu
                                            lassen. Diese Einwilligung kann ich jederzeit widerrufen, etwa durch einen
                                            Brief an die oben genannte Adresse oder durch eine E-Mail an
                                            <a href="mailto:widerruf@treaction.net">widerruf@treaction.net</a>.
                                            Anschließend wird jede werbliche Nutzung unterbleiben.</small>
                                    </label>
                                </div>
                            </div>

                            <div class="mb-8">
                                <input class="btn btn-primary  btn-block btn-submit" id="bt-snd-nw-apikey" type="submit">
                            </div>

                        </form>

                        <br>
                        <div class="alert alert-nw-cus text-hide" role="alert"><i class="fa fa-exclamation-circle"></i>&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End:Get APIKey ( New / Existing) for User -->

    <!--Begin: Footer -->
    <div class="tr_footer text-center text-muted">
        <p>&copy; 2020 treaction AG</p>
        <ul class="list-inline">
            <li class="list-inline-item">
                <a target="_blank" href="https://treaction.net/rechtliches/#impressum">Imprint</a>
            </li>
            <li class="list-inline-item">
                <a target="_blank" href="https://treaction.net/rechtliches/#agb">Conditions</a>
            </li>
            <li class="list-inline-item">
                <a target="_blank" href="https://treaction.net/rechtliches/#datenschutz">Privacy</a>
            </li>
            <li class="list-inline-item">
                <a target="_blank" href="https://treaction.net/kontakt/">Contact</a>
            </li>
        </ul>
    </div>
    <!--End: footer-->

    <script>
        $(document).ready(function () {

            let current_url_path = $(location).attr('href');
            let url = new URL(current_url_path);
            let error = url.searchParams.get('error');

            /**
             * ASCII to Unicode (decode Base64 to original data)
             */
            function atou(b64) {
                return decodeURIComponent(escape(atob(b64)));
            }

            /**
             * Unicode to ASCII (encode data to Base64)
             */
            function utoa(data) {
                return btoa(unescape(encodeURIComponent(data)));
            }

            /**
             * Jquery Generate APIKey for new customer
             */
            $('#bt-snd-nw-apikey').click(function (e) {
                $('.alert-nw-cus').empty().removeClass('alert-success').removeClass('alert-danger').addClass('text-hide');
                //$ ( '#bt-snd-nw-apikey' ).hide ();
                let salutation = $('#nw-cus-sal').val();
                let firstname = $('#nw-cus-fstnme').val();
                let lastname = $('#nw-cus-lstnme').val();
                let email = $('#nw-cus-em').val();
                let permission = $('#nw-cus-permission').is(':checked');

                let formData = {
                    'salutation': salutation,
                    'firstName': escape(decodeURIComponent(firstname)),
                    'lastName': escape(decodeURIComponent(lastname)),
                    'email': email,
                    'permission': permission,
                };

                $('#btn_test').on('click', function () {
                    let form = $('<form></form>');
                    form.attr('action', '{$oPlugin->cBackendUrl}');
                    form.attr('method', 'post');
                    form.append($('<input type="hidden" name="api_key" value="' + input_api_key.val() + '">'));
                    form.append($('<input type="hidden" name="acc_number" value="' + input_acc_no.val() + '">'));
                    form.append($('#newsletter_hook_id'));
                    form.append($('#ecommerce_hook_id'));
                    form.append($('<input type="hidden" name="acc_verified" value="1">'));
                    form.appendTo('body').submit();
                });

                if (!email) {
                    $('.alert-nw-cus').append('Bitte geben Sie den E-Mail ein').addClass('alert-danger ').removeClass('text-hide');
                    $('#nw-cus-em').focus();
                    $('#bt-snd-nw-apikey').show();
                    return false;
                }
                if (!firstname) {
                    $('.alert-nw-cus').append('Bitte geben Sie den Vorname ein').addClass('alert-danger ').removeClass('text-hide');
                    $('#nw-cus-fstnme').focus();
                    $('#bt-snd-nw-apikey').show();
                    return false;
                }
                if (!lastname) {
                    $('.alert-nw-cus').append('Bitte geben Sie den Nachname ein').addClass('alert-danger ').removeClass('text-hide');
                    $('#nw-cus-lstnme').focus();
                    $('#bt-snd-nw-apikey').show();
                    return false;
                }

                if (!permission) {
                    $('.alert-nw-cus')
                        .append('Bitte akzeptieren Sie die Allgemeinen Geschäftsbedingungen und Datenschutzbestimmungen')
                        .addClass('alert-danger ').removeClass('text-hide');
                    $('#nw-cus-permission').focus();
                    $('#bt-snd-nw-apikey').show();
                    return false;
                }

                let formJson = JSON.stringify(formData);
                console.log(formJson);
                let formbtoa = btoa(formJson);

                $.ajax({
                    url: "https://dev-api-in-one.net/general/account/create",
                    type: 'POST',
                    async: false,
                    timeout: 0,
                    header: {
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE',
                        'Access-Control-Allow-Headers': 'access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer',
                        'Access-Control-Max-Age': '86400'
                    },
                    data: formbtoa,
                    beforeSend: function (xhr) {
                        $('.alert-nw-cus').removeClass('alert-success').removeClass('alert-danger').addClass('text-hide');
                    },
                    success: function (data) {
                        let response = (data);
                        if (response.status) {
                            $('.alert-nw-cus').append(response.message).addClass('alert-success ').removeClass('text-hide');
                        } else {
                            $('.alert-nw-cus').append(response.message).addClass('alert-danger ').removeClass('text-hide');
                        }
                    }
                });
                return false;
            });

        });
    </script>
</div>