CREATE TABLE xplugin_treaction_4_data (
    kID                    INT                     NOT NULL,
    cAccountNumber         VARCHAR(191) DEFAULT '' NOT NULL,
    cAPIKey                VARCHAR(191) DEFAULT '' NOT NULL,
    cNewsletterHookId      VARCHAR(191) DEFAULT '' NOT NULL,
    cECommerceHookId       VARCHAR(191) DEFAULT '' NOT NULL,
    bAccountStatusVerified INT(1) DEFAULT 0 NOT NULL,
    PRIMARY KEY (kID)
);

INSERT INTO xplugin_treaction_4_data (kID)
VALUES (1);

UPDATE temailvorlage SET cAktiv = 'N' WHERE cModulId = 'core_jtl_newsletteranmelden';