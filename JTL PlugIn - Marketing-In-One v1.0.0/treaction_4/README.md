# MVP Shop Plugin

## Installation

1. Login to your JTL-Shop 4 backend

2. Select "Plugins" -> "Pluginverwaltung"

3. Upload your *.zip file by choosing "Upload"

4. After uploading hit "Verfuegbar", check the box and click "Install"

5. Go to your plugin configuration page from here by clicking "Activated" -> "MVP Shop Plugin" -> "Settings"

6. Access your plugin configuration page from navbar by clicking "Plugins" -> "MVP Shop Plugin"

## Configuration and Setup

If you don't have a Marketing-In-One Account / APIKey yet, chose the tab "Order APIKey", provide the necessary information and confirm the email which is sent to you immediately after clicking the "Daten absenden" button

1. If you already have registered a Marketing-In-One Account / APIKey, choose the tab "Account Information" and provide your credentials here.

2. After providing APIKey and Account Number hit "Verify APIKey"

3. Once your credentials have been verified, you'll be able to choose your "NewsLetter Webhook" and "Ecommerce Webhook". Feel free to test them, if you like, and save.

4. As soon as everything is saved you will receive the tracked data in your Marketing-In-One Account.

## Accessing your Marketing-In-One Account

https://dev-campaign-in-one.net/