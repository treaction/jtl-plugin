# README #

## JTL-Plugin von Eloquium ##

### What is this repository for? ###

* Erste Version des JTL-Plugin von Eloquium
  
* Version 1.0.0

* KONTAKT
 
    Florian Mühe
    
    eloquium GmbH
    
    Güterstraße 2
    
    64807 Dieburg
    
    Tel.: +49-6071-95900-20
    
    Fax.: +49-6071-95900-49
    
    E-Mail: support@eloquium.de
    
    www.eloquium.de

